package com.designpaterns;

public class CaliforniaPizzaStore extends PizzaStore {
    @Override
    protected Pizza createPizza(String item) {
        switch (item) {
            case "cheese":
                return new CaliforniaStyleCheesePizza();
            case "veggie":
                return new CaliforniaStyleVeggiePizza();
            case "clam":
                return new CaliforniaStyleClamPizza();
            case "pepperoni":
                return new CaliforniaStylePepperoniPizza();
            default:
                return null;
        }
    }
}
